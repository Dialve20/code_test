array_ips = []
def request_handled(ip_address):
    count = 0
    if len(array_ips) != 0:
        for ip in array_ips:
            if ip["ip"]==ip_address:
                num_request=ip["num_request"]
                ip["num_request"]=num_request+1
                count +=1
        if count == 0:
            obj = {}
            obj['ip']=ip_address
            obj['num_request']=1
            array_ips.append(obj)
    else:
        obj = {}
        obj['ip']=ip_address
        obj['num_request']=1
        array_ips.append(obj)
    return True

def top100():
    top = sorted(array_ips, key=lambda x: x['num_request'], reverse=True)
    return top[0:100]

def clear():
    array_ips = []
    return True